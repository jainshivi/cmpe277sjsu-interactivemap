//
//  MapImageView.m
//  Map
//
//  Created by Gunja Agrawal on 11/3/15.
//  Copyright (c) 2015 SJSU. All rights reserved.
//

#import "MapImageView.h"

@implementation MapImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void)updateUserLocation:(NSString *) userLocation {
    userCurrentLocation = userLocation;
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan....");
    CGPoint touchPoint = [[touches anyObject] locationInView:self];
    NSLog(@"X location: %f", touchPoint.x);
    NSLog(@"Y Location: %f",touchPoint.y);
    
    [self showAlert:touchPoint:userCurrentLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesMoved....");
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesEnded....");
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesCancelled....");
}

-(void)showAlert:(CGPoint)touchPoint:(NSString*)userLocation{
    
    NSString *title, *address, *building, *distance;
    
    if(touchPoint.x >= 180.000000 && touchPoint.x <= 275.000000
       && touchPoint.y >= 100.000000  && touchPoint.y <= 147.000000) {
        address = @"San José State University Charles W. Davidson College of Engineering, 1 Washington Square, San Jose, CA 95112";
        distance = [self calculateDistance:userLocation:address];
        title =[NSString stringWithFormat:@"%@,  %@", @"Engineering Building", distance];
        building = @"Engg.png";
    }
    else if(touchPoint.x >= 15.000000 && touchPoint.x <= 80.000000
            && touchPoint.y >= 220.000000  && touchPoint.y <= 250.000000) {
        address = @"Dr. Martin Luther King, Jr. Library, 150 East San Fernando Street, San Jose, CA 95112";
        distance = [self calculateDistance:userLocation:address];
        title =[NSString stringWithFormat:@"%@,  %@", @"King Library", distance];
        building = @"Library.png";
    }
    else if(touchPoint.x >= 80.000000 && touchPoint.x <= 155.000000
            && touchPoint.y >= 365.000000  && touchPoint.y <= 395.000000) {
        address = @"Yoshihiro Uchida Hall, San Jose, CA 95112";
        distance = [self calculateDistance:userLocation:address];
        title =[NSString stringWithFormat:@"%@,  %@", @"Yoshihiro Uchida Hall", distance];
        building = @"YUH.png";
    }
    else if(touchPoint.x >= 220.000000 && touchPoint.x <= 310.000000
            && touchPoint.y >= 150.000000  && touchPoint.y <= 190.000000) {
        address = @"Student Union Building, San Jose, CA 95112";
        distance = [self calculateDistance:userLocation:address];
        title =[NSString stringWithFormat:@"%@,  %@", @"Student Union", distance];
        building = @"SU.png";
    }
    else if(touchPoint.x >= 345.000000 && touchPoint.x <= 415.000000
            && touchPoint.y >= 140.000000  && touchPoint.y <= 166.000000) {
        address = @"Boccardo Business Complex, San Jose, CA 95112";
        distance = [self calculateDistance:userLocation:address];
        title =[NSString stringWithFormat:@"%@,  %@", @"BBC", distance];
        building = @"BBC.png";
    }
    else if(touchPoint.x >= 225.000000 && touchPoint.x <= 330.000000
            && touchPoint.y >= 410.000000  && touchPoint.y <= 440.000000) {
        address = @"San Jose State University South Garage, 330 South 7th Street, San Jose, CA 95112";
        distance = [self calculateDistance:userLocation:address];
        title =[NSString stringWithFormat:@"%@,  %@", @"South Parking Garage", distance];
        building = @"Garage.png";
    }
    
    if(title != nil){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:title
                              message:address
                              delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 200, 100)];
        
        NSString *path = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:building]];
        UIImage *buildingImg = [[UIImage alloc] initWithContentsOfFile:path];
        [imageView setImage:buildingImg];
        
        [alert addSubview:imageView];
        [alert setValue:imageView forKey:@"accessoryView"];
        [alert show];
        
    }
}

-(NSString*)calculateDistance:(NSString*)origin:(NSString*)destination{
    
    NSString *urlPath = [NSString stringWithFormat:@"/maps/api/distancematrix/json?origins=%@&destinations=%@&mode=walking&language=en-EN&sensor=false" ,origin , destination];
    NSURL *url = [[NSURL alloc]initWithScheme:@"http" host:@"maps.googleapis.com" path:urlPath];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    [request setURL:url];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse *response ;
    NSError *error;
    NSData *data;
    data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSMutableDictionary *jsonDict= (NSMutableDictionary*)[NSJSONSerialization  JSONObjectWithData:data options:kNilOptions error:&error];
    NSMutableDictionary *newdict=[jsonDict valueForKey:@"rows"];
    NSArray *elementsArr=[newdict valueForKey:@"elements"];
    NSArray *arr=[elementsArr objectAtIndex:0];
    NSDictionary *dict=[arr objectAtIndex:0];
    
    // Get Distance
    NSMutableDictionary *distanceDict=[dict valueForKey:@"distance"];
    NSLog(@"distance:%@",[distanceDict valueForKey:@"text"]);
    
    // Get Time
    NSMutableDictionary *durationDict=[dict valueForKey:@"duration"];
    NSLog(@"duration:%@",[durationDict valueForKey:@"text"]);
    
    NSString *combined = [NSString stringWithFormat:@"Distance: %@, Time: %@", [distanceDict valueForKey:@"text"], [durationDict valueForKey:@"text"]];
    
    NSLog(combined);
    return combined;
}


@end
