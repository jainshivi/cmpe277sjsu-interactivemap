//
//  main.m
//  MapLab3
//
//  Created by Gunja Agrawal on 11/5/15.
//  Copyright (c) 2015 SJSU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Lab3AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([Lab3AppDelegate class]));
    }
}
