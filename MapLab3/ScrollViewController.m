//
//  ScrollViewController.m
//  MapLab3
//
//  Created by Gunja Agrawal on 11/5/15.
//  Copyright (c) 2015 SJSU. All rights reserved.
//

#import "ScrollViewController.h"
#import "MapImageView.h"

@interface ScrollViewController ()

@end

@implementation ScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageV;
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self search:searchText];
}

-(void)search:(NSString *)searchText
{
    NSString *text = [searchText lowercaseString];
    
    if(mapLoc[text])
    {
        [self.view endEditing:TRUE];
        NSValue *val = mapLoc[text];
        CGPoint point = [val CGPointValue];
        [self moveToCenter:point];
    }

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self search:searchBar.text];
    [searchBar resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [searchBar resignFirstResponder];
}

- (void) searchResults
{
    mapLoc = @{
        @"eng" : [NSValue valueWithCGPoint:CGPointMake(227.5, 130)],
        @"engineering building" : [NSValue valueWithCGPoint:CGPointMake(227.5, 130)],
        @"king" : [NSValue valueWithCGPoint:CGPointMake(47.5,235)],
        @"king library" : [NSValue valueWithCGPoint:CGPointMake(47.5,235)],
        @"yuh" : [NSValue valueWithCGPoint:CGPointMake(117.5,380)],
        @"yoshihiro uchida hall" : [NSValue valueWithCGPoint:CGPointMake(117.5,380)],
        @"su" : [NSValue valueWithCGPoint:CGPointMake(265,187.5)],
        @"student union" : [NSValue valueWithCGPoint:CGPointMake(265,187.5)],
        @"bbc" : [NSValue valueWithCGPoint:CGPointMake(385,155)],
        @"boccardo business center" : [NSValue valueWithCGPoint:CGPointMake(385,155)],
        @"south parking garage" : [NSValue valueWithCGPoint:CGPointMake(277.5,422.5)],
        @"spg" : [NSValue valueWithCGPoint:CGPointMake(277.5,422.5)],
        @"spm" : [NSValue valueWithCGPoint:CGPointMake(277.5,422.5)]
    };
}


// Delegate method
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation* loc = [locations lastObject]; // locations is guaranteed to have at least one object
    float latitude = loc.coordinate.latitude;
    float longitude = loc.coordinate.longitude;

    //(page==1)?(buttonPrev.hidden=TRUE):(buttonPrev.hidden=FALSE);
        
    float x = floorf(imageV.frame.size.width * (fabs(longitude) - 121.886478)/(121.876243 - 121.886478));
    float y = floorf(imageV.frame.size.height - (imageV.frame.size.height * (fabs(latitude)-37.331361)/(37.338800-37.331361)));

    if (x < 0.0) {
        x = 0.0;
    } else if(x > imageV.frame.size.width) {
        x = imageV.frame.size.width;
    }
    
    if (y < 0.0) {
        y = 0.0;
    } else if (y > imageV.frame.size.height) {
        y = imageV.frame.size.height;
    }
    
    if (currentLocation.x != x || currentLocation.y != y) {
        [self updateLocation: CGPointMake(x,y)];
        currentLocation = CGPointMake(x,y);
    }
    
    // Update the user location.
    NSString *userLocation_lat = @(latitude).stringValue;
    NSString *userLocation_lon = @(longitude).stringValue;
    NSString *userLocation = [NSString stringWithFormat:@"%@,%@", userLocation_lat, userLocation_lon];
    [imageV updateUserLocation:userLocation];
}


-(void)moveToCenter:(CGPoint)touchPoint{
   
    scrollV.zoomScale = 2.0;
    
    CGPoint zTouchPoint = CGPointMake(touchPoint.x * scrollV.zoomScale, touchPoint.y * scrollV.zoomScale);
    
    [scrollV scrollRectToVisible:CGRectMake(zTouchPoint.x - roundf(scrollV.frame.size.width/2.),
                                            zTouchPoint.y - roundf(scrollV.frame.size.height/2.),
                                            scrollV.frame.size.width,
                                            scrollV.frame.size.height)
     animated:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self  searchResults];
    
    //scrollV.delegate = self;
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    searchBar.delegate = self;
    
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [locationManager startUpdatingLocation];
   // [locationManager requestWhenInUseAuthorization];
    
    // creat and configure the pan gesture
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureDetected:)];
    
    [panGestureRecognizer setDelegate:self];
    [imageV addGestureRecognizer:panGestureRecognizer];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(scalePiece:)];
    [pinchGesture setDelegate:self];
    [imageV addGestureRecognizer:pinchGesture];
    
    location=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"location.png"]];
    [imageV addSubview:location];
    
    mapFrameCoordinates = imageV.frame.origin;
}


- (void)scalePiece:(UIPinchGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGFloat newScale = gestureRecognizer.scale * scrollV.zoomScale;
        CGFloat minScale = MIN(scrollV.frame.size.width / imageV.frame.size.width, scrollV.frame.size.height / imageV.frame.size.height);
        
        if (newScale < minScale) return;
        
        [gestureRecognizer view].transform = CGAffineTransformScale([[gestureRecognizer view] transform], [gestureRecognizer scale], [gestureRecognizer scale]);
        [gestureRecognizer setScale:1];
    }
}


- (void)panGestureDetected:(UIPanGestureRecognizer *)recognizer
{
    UIGestureRecognizerState state = [recognizer state];
    
    if (state == UIGestureRecognizerStateBegan || state == UIGestureRecognizerStateChanged)
    {
        CGPoint translation = [recognizer translationInView:recognizer.view];
        
        [recognizer.view setTransform:CGAffineTransformTranslate(recognizer.view.transform, translation.x, translation.y)];
        
        [recognizer setTranslation:CGPointZero inView:recognizer.view];
    }
}


- (void) updateLocation:(CGPoint)loc
{
    CGRect theMoveToPosition = CGRectMake(loc.x / scrollV.zoomScale, loc.y / scrollV.zoomScale, location.frame.size.width, location.frame.size.height);
    
    [UIView animateWithDuration:0 delay:0 options:UIViewAnimationCurveEaseInOut
					 animations:^ {
                         location.image = [UIImage imageNamed:@"location.png"];
						 location.frame = theMoveToPosition;
					 }completion:^(BOOL finished) {
                         if (finished) {
                             location.image = [UIImage imageNamed:@"location.png"];
                         }
                     }];
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        NSString *lonStr = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        NSString *lanStr = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
        NSLog(lonStr);
        NSLog(lanStr);
    }
}


- (void)loadView
{
    [super loadView];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"loadView....");
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidLayoutSubviews{
    scrollV.contentSize = imageV.frame.size;
    scrollV.minimumZoomScale=0.5f;
    scrollV.maximumZoomScale=6.0f;
    scrollV.clipsToBounds = YES;
    //scrollV.contentSize=CGSizeMake(1280, 960);
    scrollV.delegate=self;
}

@end
