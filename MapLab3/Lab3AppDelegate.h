//
//  Lab3AppDelegate.h
//  MapLab3
//
//  Created by Gunja Agrawal on 11/5/15.
//  Copyright (c) 2015 SJSU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Lab3AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
