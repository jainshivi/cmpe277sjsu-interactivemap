//
//  ScrollViewController.h
//  MapLab3
//
//  Created by Gunja Agrawal on 11/5/15.
//  Copyright (c) 2015 SJSU. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MapImageView.h"


@interface ScrollViewController : UIViewController<UISearchBarDelegate, UIScrollViewDelegate, CLLocationManagerDelegate> {

    IBOutlet UIScrollView *scrollV;
    IBOutlet MapImageView *imageV;
    IBOutlet UIImageView *location;
    
    IBOutlet UISearchBar *searchBar;
    IBOutlet CLLocationManager *locationManager;

    IBOutlet UILabel *latLabel;
    IBOutlet UILabel *longLabel;
    NSDictionary *mapLoc;
    
    CGPoint currentLocation;
    CGPoint mapFrameCoordinates;
}

@end

