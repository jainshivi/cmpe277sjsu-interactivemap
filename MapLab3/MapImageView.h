//
//  MapImageView.h
//  MapLab3
//
//  Created by Gunja Agrawal on 11/5/15.
//  Copyright (c) 2015 SJSU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapImageView : UIImageView {
    NSString * userCurrentLocation;
}

-(void)showAlert:(CGPoint)touchPoint;
-(void)updateUserLocation:(NSString *) userLocation;

@end
